﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Vape.ViewModels
{
    public class VendorImportViewModel
    {
        [Display(Name = "Vendor Master")]
        [Required]
        public IFormFile File { get; set; }
    }
}
