﻿using CPh.Vape.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Vape.ViewModels
{
    public class ApproverViewModel
    {
        public IEnumerable<User> ApproversList { get; set; }
        [Display(Name = "Select User")]
        public Guid SelectedApproverId { get; set; }
        public Guid EvaluationId { get; set; }
    }
}
