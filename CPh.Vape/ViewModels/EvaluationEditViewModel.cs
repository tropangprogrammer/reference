﻿using CPh.Vape.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using static CPh.Vape.Models.Evaluation;

namespace CPh.Vape.ViewModels
{
    public class EvaluationEditViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Evaluation Year")]
        public int EvaluationYear { get; set; }
        [Display(Name = "Evaluation Type")]
        public Guid SelectedEvaluationTypeId { get; set; }
        public IEnumerable<SelectListItem> EvaluationTypeList { get; set; }
        [Display(Name = "Product Type")]
        public ProductType ProductType { get; set; }
        [Display(Name = "Review Period")]
        public ReviewFrequency ReviewPeriod { get; set; }
        public int Quality { get; set; }
        [Display(Name = "Price Changes")]
        public int PriceChanges { get; set; }
        public int Delivery { get; set; }
        public int Service { get; set; }
        public string Comments { get; set; }
    }
}
