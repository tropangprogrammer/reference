﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CPh.Vape.Models;
using static CPh.Vape.Models.Evaluation;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CPh.Vape.ViewModels
{
    public class EvaluationCreateViewModel
    {
        [Display(Name = "Evaluation Type")]
        public Guid SelectedEvaluationTypeId { get; set; }
        public IEnumerable<SelectListItem> EvaluationTypeList { get; set; }
        [Display(Name = "Product Type")]
        public ProductType ProductType { get; set; }
        [Range(0, 10)]
        public int Quality { get; set; }
        [Range(0, 10)]
        [Display(Name = "Price Changes")]
        public int PriceChanges { get; set; }
        [Range(0, 10)]
        public int Delivery { get; set; }
        [Range(0, 10)]
        public int Service { get; set; }
        public string Comments { get; set; }
        public IEnumerable<Vendor> VendorList { get; set; }
        [Display(Name = "Vendor")]
        public Guid SelectedVendorId { get; set; }
        [Display(Name = "Year")]
        public int EvaluationYear { get; set; }
        [Display(Name = "Review Period")]
        public ReviewFrequency ReviewFrequency { get; set; }
    }
}
