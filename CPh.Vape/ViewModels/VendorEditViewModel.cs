﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Vape.ViewModels
{
    public class VendorEditViewModel
    {
        public Guid Id { get; set; }
        [Display(Name = "Vendor Code")]
        [Required]
        public string VendorCode { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }
        [Required]
        public string Country { get; set; }
        public string Website { get; set; }
        [Required]
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }
        [Required]
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
    }
}
