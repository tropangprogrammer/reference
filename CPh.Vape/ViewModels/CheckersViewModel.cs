﻿using CPh.Vape.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Vape.ViewModels
{
    public class CheckersViewModel
    {
        public IEnumerable<User> CheckersList { get; set; }
        [Display(Name = "Select User")]
        public Guid SelectedCheckerId { get; set; }
        public Guid EvaluationId { get; set; }
    }
}
