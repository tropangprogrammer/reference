﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CPh.AdQuery;

namespace CPh.Vape.ViewModels
{
    public class UserSearchViewModel
    {
        [Display(Name = "Global ID or Name")]
        [Required]
        public string Query { get; set; }
        public IEnumerable<AdUser> Users { get; set; }
        [Display(Name = "Global ID")]
        public string SelectedGlobalId { get; set; }
        [Display(Name = "Name")]
        public string SelectedName { get; set; }
    }
}
