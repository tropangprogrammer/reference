﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CPh.Vape.Models;
using CPh.Vape.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;

namespace CPh.Vape.Controllers
{
    [Authorize]
    public class VendorsController : Controller
    {
        private readonly VapeDbContext _context;

        public VendorsController(VapeDbContext context)
        {
            _context = context;
        }

        // GET: Vendors
        public async Task<IActionResult> Index()
        {
            return View(await _context.Vendors.ToListAsync());
        }

        // GET: Vendors/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var vendor = await _context.Vendors.Where(m => m.Id == id).Include(x => x.Contact).SingleOrDefaultAsync();

            if (vendor == null)
            {
                return NotFound();
            }

            return View(vendor);
        }

        // GET: Vendors/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Vendors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(VendorCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var vendor = new Vendor
                {
                    VendorCode = viewModel.VendorCode,
                    Name = viewModel.Name,
                    Address = viewModel.Address,
                    Country = viewModel.Country,
                    PostalCode = viewModel.PostalCode,
                    Website = viewModel.Website
                };
                var vendorContact = new VendorContact
                {
                    Name = viewModel.ContactName,
                    Email = viewModel.Email,
                    Fax = viewModel.Fax,
                    Telephone = viewModel.Telephone,
                    Vendor = vendor
                };
                vendor.Contact = vendorContact;
                _context.Add(vendor);
                _context.Add(vendorContact);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        // GET: Vendors/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vendor = await _context.Vendors.Where(m => m.Id == id)
                .Include(x => x.Contact).SingleOrDefaultAsync();

            if (vendor == null)
            {
                return NotFound();
            }

            var viewModel = new VendorEditViewModel
            {
                Address = vendor.Address,
                Country = vendor.Country,
                Name = vendor.Name,
                PostalCode = vendor.PostalCode,
                VendorCode = vendor.VendorCode,
                Website = vendor.Website,
            };

            if (vendor.Contact != null)
            {
                viewModel.ContactName = vendor.Contact.Name;
                viewModel.Email = vendor.Contact.Email;
                viewModel.Fax = vendor.Contact.Fax;
                viewModel.Telephone = vendor.Contact.Telephone;
            }

            return View(viewModel);
        }

        // POST: Vendors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, VendorEditViewModel viewModel)
        {
            if (id != viewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var vendor = await _context.Vendors.Where(x => x.Id == id).Include(x => x.Contact).SingleOrDefaultAsync();
                    vendor.Address = viewModel.Address;
                    vendor.Country = viewModel.Country;
                    vendor.Name = viewModel.Name;
                    vendor.PostalCode = viewModel.PostalCode;
                    vendor.VendorCode = viewModel.VendorCode;
                    vendor.Website = viewModel.Website;

                    if (vendor.Contact == null)
                    {
                        var contact = new VendorContact
                        {
                            Vendor = vendor,
                            Email = viewModel.Email,
                            Fax = viewModel.Fax,
                            Name = viewModel.ContactName,
                            Telephone = viewModel.Telephone
                        };
                        _context.Add(contact);
                    }
                    else
                    {
                        vendor.Contact.Email = viewModel.Email;
                        vendor.Contact.Fax = viewModel.Fax;
                        vendor.Contact.Name = viewModel.ContactName;
                        vendor.Contact.Telephone = viewModel.Telephone;
                    }

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VendorExists(viewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        // GET: Vendors/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vendor = await _context.Vendors
                .SingleOrDefaultAsync(m => m.Id == id);
            if (vendor == null)
            {
                return NotFound();
            }

            return View(vendor);
        }

        // POST: Vendors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var vendor = await _context.Vendors.SingleOrDefaultAsync(m => m.Id == id);
            _context.Vendors.Remove(vendor);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public IActionResult Import()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Import(VendorImportViewModel viewModel)
        {
            var excelFile = new ExcelPackage(viewModel.File.OpenReadStream());
            var ws = excelFile.Workbook.Worksheets[1];
            
            for (int i = 2; i <= ws.Dimension.End.Row; i++)
            {
                var vendorCode = ws.Cells[i, 1].GetValue<string>();
                var result = _context.Vendors.Any(x => x.VendorCode == vendorCode); // check if vendor exists

                if (result)
                    continue; // skip to next row

                var vendor = new Vendor
                {
                    Address = $"{ws.Cells[i, 21].Value}{ws.Cells[i, 22].Value}{ws.Cells[i, 23].Value}{ws.Cells[i, 24].Value}",
                    Country = ws.Cells[i, 29].GetValue<string>(),
                    Name = $"{ws.Cells[i, 8].Value}{ws.Cells[i, 9].Value}{ws.Cells[i, 10].Value}{ws.Cells[i, 11].Value}",
                    PostalCode = ws.Cells[i, 20].GetValue<string>(),
                    VendorCode = ws.Cells[i,1].GetValue<string>()
                };
                _context.Add(vendor);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction(nameof(Index));
        }

        private bool VendorExists(Guid id)
        {
            return _context.Vendors.Any(e => e.Id == id);
        }
    }
}
