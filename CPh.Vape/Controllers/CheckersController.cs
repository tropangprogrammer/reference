﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CPh.Vape.Models;
using CPh.Vape.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace CPh.Vape.Controllers
{
    [Authorize]
    public class CheckersController : Controller
    {
        private readonly VapeDbContext _context;

        public CheckersController(VapeDbContext context)
        {
            _context = context;
        }

        // GET: Checkers/Create
        public async Task<IActionResult> Create(Guid id)
        {
            var checkers = await _context.Users.ToListAsync();
            var viewModel = new CheckersViewModel
            {
                CheckersList = checkers,
                EvaluationId = id
            };

            return View(viewModel);
        }

        // POST: Checkers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CheckersViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var checker = new Checker
                {
                    Evaluation = await _context.Evaluations.SingleAsync(x => x.Id == viewModel.EvaluationId),
                    User = await _context.Users.SingleAsync(x => x.Id == viewModel.SelectedCheckerId)
                };
                _context.Add(checker);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Evaluations", new { id = checker.Evaluation.Id });
            }
            return View(viewModel);
        }
        
        public async Task<IActionResult> Delete(Guid id)
        {
            var checker = await _context.Checkers.SingleOrDefaultAsync(m => m.Id == id);
            await _context.Entry(checker).Reference(x => x.Evaluation).LoadAsync();
            var evaluationId = checker.Evaluation.Id;
            _context.Checkers.Remove(checker);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Evaluations", new { id = evaluationId });
        }

        private bool CheckerExists(Guid id)
        {
            return _context.Checkers.Any(e => e.Id == id);
        }
    }
}
