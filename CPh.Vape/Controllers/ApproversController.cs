﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CPh.Vape.Models;
using CPh.Vape.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace CPh.Vape.Controllers
{
    [Authorize]
    public class ApproversController : Controller
    {
        private readonly VapeDbContext _context;

        public ApproversController(VapeDbContext context)
        {
            _context = context;
        }

        // GET: Approvers/Create
        public async Task<IActionResult> Create(Guid id)
        {
            var approvers = await _context.Users.ToListAsync();
            var viewModel = new ApproverViewModel
            {
                ApproversList = approvers,
                EvaluationId = id
            };
            return View(viewModel);
        }

        // POST: Approvers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ApproverViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var approver = new Approver
                {
                    Evaluation = await _context.Evaluations.SingleAsync(x => x.Id == viewModel.EvaluationId),
                    User = await _context.Users.SingleAsync(x => x.Id == viewModel.SelectedApproverId)
                };
                _context.Add(approver);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Evaluations", new { id = approver.Evaluation.Id });
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Delete(Guid id)
        {
            var approver = await _context.Approvers.SingleOrDefaultAsync(m => m.Id == id);
            await _context.Entry(approver).Reference(x => x.Evaluation).LoadAsync();
            var evaluationId = approver.Evaluation.Id;
            _context.Approvers.Remove(approver);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "Evaluations", new { id = evaluationId });
        }

        private bool ApproverExists(Guid id)
        {
            return _context.Approvers.Any(e => e.Id == id);
        }
    }
}
