﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CPh.AdQuery.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CPh.Vape.Models;
using CPh.Vape.ViewModels;
using Microsoft.AspNetCore.Authorization;
using OfficeOpenXml;
using System.IO;
using System.Text;

namespace CPh.Vape.Controllers
{
    [Authorize]
    public class EvaluationsController : Controller
    {
        private readonly VapeDbContext _context;

        public EvaluationsController(VapeDbContext context)
        {
            _context = context;
        }

        // GET: Evaluations
        public async Task<IActionResult> Index()
        {
            var list = await _context.Evaluations
                .Include(x => x.Vendor)
                .Include(x => x.EvaluationType)
                .ToListAsync();
            return View(list);
        }

        // GET: Evaluations/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var evaluations = _context.Evaluations.Include(x => x.Preparer).Include(x => x.Vendor)
                .Include(x => x.Checkers).ThenInclude(x => x.User)
                .Include(x => x.Approvers).ThenInclude(x => x.User)
                .Include(x => x.EvaluationType);
            var evaluation = await evaluations.Where(x => x.Id == id).SingleOrDefaultAsync();

            if (evaluation == null)
            {
                return NotFound();
            }

            return View(evaluation);
        }

        // GET: Evaluations/Create
        public async Task<IActionResult> Create()
        {
            var vendorList = await _context.Vendors.ToListAsync();
            var evaluationTypesList = await _context.EvaluationTypes.Select(x => new SelectListItem
            {
                Text = x.Description,
                Value = x.Id.ToString()
            }).ToListAsync();

            var viewModel = new EvaluationCreateViewModel
            {
                VendorList = vendorList,
                EvaluationTypeList = evaluationTypesList
            };
            return View(viewModel);
        }

        // POST: Evaluations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EvaluationCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var globalId = User.UserDetails().Name;
                var currentUser = await _context.Users.SingleOrDefaultAsync(m => m.GlobalId == globalId);
                int serial = 1;
                if (await _context.Evaluations.AnyAsync())
                {
                    serial = await _context.Evaluations.MaxAsync(x => x.Serial) + 1;
                }

                var evaluationType = await _context.EvaluationTypes.SingleOrDefaultAsync(m => m.Id == viewModel.SelectedEvaluationTypeId);

                var evaluation = new Evaluation
                {
                    Vendor = await _context.Vendors.SingleOrDefaultAsync(m => m.Id == viewModel.SelectedVendorId),
                    Comments = viewModel.Comments,
                    Delivery = viewModel.Delivery,
                    PriceChanges = viewModel.PriceChanges,
                    Quality = viewModel.Quality,
                    Service = viewModel.Service,
                    Preparer = currentUser,
                    Date = DateTime.Now,
                    Serial = serial,
                    Product = viewModel.ProductType,
                    EvaluationType = evaluationType,
                    EvaluationYear = viewModel.EvaluationYear,
                    ReviewFreq = viewModel.ReviewFrequency
                };

                var evaluationCriterion = new EvaluationCriterion
                {
                    Evaluation = evaluation,
                    DeliveryPercent = evaluationType.DeliveryPercent,
                    PriceChangesPercent = evaluationType.PriceChangesPercent,
                    QualityPercent = evaluationType.QualityPercent,
                    ServicePercent = evaluationType.ServicePercent
                };

                evaluation.EvaluationCriterion = evaluationCriterion;

                _context.Evaluations.Add(evaluation);
                _context.EvaluationCriteria.Add(evaluationCriterion);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", new { id = evaluation.Id });
            }
            return View(viewModel);
        }

        // GET: Evaluations/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluation = await _context.Evaluations.SingleOrDefaultAsync(m => m.Id == id);
            if (evaluation == null)
            {
                return NotFound();
            }

            var evaluationTypesList = await _context.EvaluationTypes.Select(x => new SelectListItem
            {
                Text = x.Description,
                Value = x.Id.ToString()
            }).ToListAsync();

            var viewModel = new EvaluationEditViewModel
            {
                Comments = evaluation.Comments,
                Delivery = evaluation.Delivery,
                EvaluationTypeList = evaluationTypesList,
                EvaluationYear = evaluation.EvaluationYear,
                Id = evaluation.Id,
                PriceChanges = evaluation.PriceChanges,
                ProductType = evaluation.Product,
                Quality = evaluation.Quality,
                ReviewPeriod = evaluation.ReviewFreq,
                Service = evaluation.Service
            };

            return View(viewModel);
        }

        // POST: Evaluations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, EvaluationEditViewModel viewModel)
        {
            if (id != viewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var evaluation = await _context.Evaluations.Where(x => x.Id == id).SingleAsync();

                    evaluation.EvaluationYear = viewModel.EvaluationYear;
                    evaluation.EvaluationType = await _context.EvaluationTypes.FindAsync(viewModel.SelectedEvaluationTypeId);
                    evaluation.Product = viewModel.ProductType;
                    evaluation.ReviewFreq = viewModel.ReviewPeriod;
                    evaluation.Quality = viewModel.Quality;
                    evaluation.PriceChanges = viewModel.PriceChanges;
                    evaluation.Delivery = viewModel.Delivery;
                    evaluation.Service = viewModel.Service;
                    evaluation.Comments = viewModel.Comments;

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EvaluationExists(viewModel.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viewModel);
        }

        // GET: Evaluations/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluation = await _context.Evaluations
                .SingleOrDefaultAsync(m => m.Id == id);
            if (evaluation == null)
            {
                return NotFound();
            }

            return View(evaluation);
        }

        // POST: Evaluations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid? id)
        {
            var evaluation = await _context.Evaluations
                .Include(x => x.Approvers)
                .Include(x => x.Checkers)
                .Include(x => x.Preparer)
                .SingleOrDefaultAsync(x => x.Id == id);
            _context.Evaluations.Remove(evaluation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Download(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var evaluations = _context.Evaluations
                .Include(x => x.Approvers).ThenInclude(x => x.User)
                .Include(x => x.Checkers).ThenInclude(x => x.User)
                .Include(x => x.Preparer)
                .Include(x => x.Vendor).ThenInclude(x => x.Contact)
                .Include(x => x.EvaluationType)
                .Include(x => x.EvaluationCriterion);
            var evaluation = await evaluations.SingleOrDefaultAsync(x => x.Id == id);

            if (evaluation == null)
            {
                return NotFound();
            }

            var fileInfo = new FileInfo("Eval-Template.xlsx");

            using (var excelFile = new ExcelPackage(fileInfo))
            {
                var outputExcel = new MemoryStream();
                var ws = excelFile.Workbook.Worksheets[1];
                ws.Cells["I2"].Value = $"VPE-{evaluation.Serial:000000}";
                ws.Cells["I3"].Value = evaluation.Date;
                ws.Cells["D8"].Value = evaluation.EvaluationType.Description;
                ws.Cells["D9"].Value = evaluation.Product.ToString();
                ws.Cells["D12"].Value = evaluation.Vendor.Name;
                ws.Cells["D13"].Value = evaluation.Vendor.Address;
                if (evaluation.Vendor.Contact != null)
                {
                    ws.Cells["D14"].Value = evaluation.Vendor.Contact.Name;
                    ws.Cells["D15"].Value = evaluation.Vendor.Contact.Telephone;
                    ws.Cells["D16"].Value = evaluation.Vendor.Contact.Fax;
                    ws.Cells["D17"].Value = evaluation.Vendor.Contact.Email;
                }
                ws.Cells["D18"].Value = evaluation.Vendor.Website;
                ws.Cells["E25"].Value = evaluation.Quality;
                ws.Cells["E26"].Value = evaluation.PriceChanges;
                ws.Cells["E27"].Value = evaluation.Delivery;
                ws.Cells["E28"].Value = evaluation.Service;
                ws.Cells["B33"].Value = evaluation.Comments;
                ws.Cells["B42"].Value = evaluation.Preparer.Name;
                ws.Cells["D25"].Value = (float)evaluation.EvaluationCriterion.QualityPercent / 100;
                ws.Cells["D26"].Value = (float)evaluation.EvaluationCriterion.PriceChangesPercent / 100;
                ws.Cells["D27"].Value = (float)evaluation.EvaluationCriterion.DeliveryPercent / 100;
                ws.Cells["D28"].Value = (float)evaluation.EvaluationCriterion.ServicePercent / 100;
                ws.Cells["F25"].Formula = "=D25*E25";
                ws.Cells["F26"].Formula = "=D26*E26";
                ws.Cells["F27"].Formula = "=D27*E27";
                ws.Cells["F28"].Formula = "=D28*E28";
                ws.Cells["F21"].Value = evaluation.EvaluationYear.ToString();
                ws.Cells["B20"].Value = evaluation.ReviewFreq == Evaluation.ReviewFrequency.First ? "1st Bi-Annual Review" : "2nd Bi-Annual Review";

                var preparerBuilder = new StringBuilder();

                foreach (var checker in evaluation.Checkers)
                {
                    preparerBuilder.AppendLine(checker.User.Name);
                    preparerBuilder.AppendLine();
                }
                ws.Cells["E42"].Value = preparerBuilder.ToString();

                var approverBuilder = new StringBuilder();

                foreach (var approver in evaluation.Approvers)
                {
                    approverBuilder.AppendLine(approver.User.Name);
                    preparerBuilder.AppendLine();
                }
                ws.Cells["I42"].Value = approverBuilder.ToString();

                excelFile.SaveAs(outputExcel);
                outputExcel.Seek(0, SeekOrigin.Begin);
                var fsResult = new FileStreamResult(outputExcel, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                fsResult.FileDownloadName = $"Evaluation-{evaluation.Vendor.Name}-{evaluation.EvaluationYear}-{evaluation.ReviewFreq}.xlsx";

                return fsResult;
            }
        }

        private bool EvaluationExists(Guid id)
        {
            return _context.Evaluations.Any(e => e.Id == id);
        }
    }
}