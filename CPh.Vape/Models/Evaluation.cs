﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CPh.Vape.Models
{
    public class Evaluation
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public int Quality { get; set; }
        [Display(Name = "Price Changes")]
        public int PriceChanges { get; set; }
        public int Delivery { get; set; }
        public int Service { get; set; }
        public int Serial { get; set; }
        public DateTime Date { get; set; }
        public string Comments { get; set; }
        public virtual Vendor Vendor { get; set; }
        public virtual User Preparer { get; set; }
        public virtual ICollection<Checker> Checkers { get; set; }
        public virtual ICollection<Approver> Approvers { get; set; }
        [Display(Name = "Evaluation Type")]
        public EvaluationType EvaluationType { get; set; }
        public ProductType Product { get; set; }
        public EvaluationCriterion EvaluationCriterion { get; set; }
        public bool Locked { get; set; }
        [Display(Name = "Review Period")]
        public ReviewFrequency ReviewFreq { get; set; }
        [Display(Name = "Evaluation Year")]
        public int EvaluationYear { get; set; }

        public enum ProductType
        {
            [Display(Name = "IT Hardware / Software Product")]
            ItProduct,
            [Display(Name = "IT Service")]
            ItService,
            [Display(Name = "Non-IT Product")]
            NonItProduct,
            [Display(Name = "Non-IT Service")]
            NonItService
        }

        public enum ReviewFrequency
        {
            [Display(Name = "1st")]
            First,
            [Display(Name = "2nd")]
            Second
        }
    }
}
