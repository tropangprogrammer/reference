﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Vape.Models
{
    public class EvaluationCriterion
    {
        public int Id { get; set; }
        public Evaluation Evaluation { get; set; }
        public int QualityPercent { get; set; }
        public int PriceChangesPercent { get; set; }
        public int DeliveryPercent { get; set; }
        public int ServicePercent { get; set; }

    }
}
