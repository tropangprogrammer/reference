﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CPh.Vape.Models
{
    public class VapeDbContext : DbContext
    {
        public VapeDbContext(DbContextOptions<VapeDbContext> options) : base(options)
        {

        }

        public DbSet<Evaluation> Evaluations { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<VendorContact> VendorContacts { get; set; }
        public DbSet<Approver> Approvers { get; set; }
        public DbSet<Checker> Checkers { get; set; }
        public DbSet<EvaluationCriterion> EvaluationCriteria { get; set; }
        public DbSet<EvaluationType> EvaluationTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendor>()
                .HasIndex(x => new { x.VendorCode })
                .IsUnique();
        }
    }
}
