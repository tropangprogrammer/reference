﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CPh.Vape.Models
{
    public class EvaluationType
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        [Display(Name = "Quality Percent")]
        [Range(0, 100)]
        public int QualityPercent { get; set; }
        [Display(Name = "Price Changes Percent")]
        [Range(0, 100)]
        public int PriceChangesPercent { get; set; }
        [Display(Name = "Delivery Percent")]
        [Range(0, 100)]
        public int DeliveryPercent { get; set; }
        [Display(Name = "Service Percent")]
        [Range(0, 100)]
        public int ServicePercent { get; set; }
        
    }
}
