﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CPh.Vape.Models
{
    public class Approver
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public virtual Evaluation Evaluation { get; set; }
        public virtual User User { get; set; }
    }
}