﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CPh.Vape.Models
{
    public class Vendor
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Display(Name = "Vendor Code")]
        public string VendorCode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Website { get; set; }
        public virtual ICollection<Evaluation> Evaluations { get; set; }
        public virtual VendorContact Contact { get; set; }
    }
}