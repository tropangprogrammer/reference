﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CPh.Vape.Migrations
{
    public partial class AddNewColumnsToEvaluation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EvaluationYear",
                table: "Evaluations",
                type: "int4",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ReviewFreq",
                table: "Evaluations",
                type: "int4",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EvaluationYear",
                table: "Evaluations");

            migrationBuilder.DropColumn(
                name: "ReviewFreq",
                table: "Evaluations");
        }
    }
}
