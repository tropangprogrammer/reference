﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace CPh.Vape.Authorization
{
    public class ActiveDirectoryRequirement : AuthorizationHandler<ActiveDirectoryRequirement>, IAuthorizationRequirement
    {
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, ActiveDirectoryRequirement requirement)
        {
        }
    }
}
