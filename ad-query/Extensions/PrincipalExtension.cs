﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CPh.AdQuery.Extensions
{
    public static class PrincipalExtension
    {
        private static string Normalize(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.Normalize(NormalizationForm.FormKC).ToLower());
        }

        public static string FullName(this IPrincipal user)
        {
            var name = Normalize($"{user.UserDetails().GivenName} {user.UserDetails().Surname}");
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(name);
        }

        public static UserPrincipal UserDetails(this IPrincipal user)
        {
            var split = AdSearch.SplitUserData(user.Identity.Name);
            var ctx = new PrincipalContext(ContextType.Domain, split.Domain);
            var userPrincipal = UserPrincipal.FindByIdentity(ctx, split.Name);

            return userPrincipal;
        }
    }
}
