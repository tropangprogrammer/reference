﻿namespace CPh.AdQuery
{
    public class AdUser
    {
        public string Name { get; set; }
        public string GlobalId { get; set; }
        public string Domain { get; set; }
        public string Email { get; set; }
    }
}