﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CPh.AdQuery
{
    public static class AdSearch
    {
        public static UserPrincipal RetrieveUserDetails(string username)
        {
            var split = SplitUserData(username);
            var ctx = new PrincipalContext(ContextType.Domain, split.Domain);
            var user = UserPrincipal.FindByIdentity(ctx, split.Name);

            return user;
        }

        public static AdUser SplitUserData(string username)
        {
            var split = username.Split('\\');
            return new AdUser
            {
                Name = split[1],
                Domain = split[0],
            };
        }

        public static List<AdUser> FindUser(string query, string domain)
        {
            var entry = new DirectoryEntry($"LDAP://{domain}");
            SearchResultCollection result;
            using (var searcher = new DirectorySearcher(entry))
            {
                searcher.Filter = string.Format("(&(objectClass=person)(|(cn=*{0}*)(displayName=*{0}*)))", query);

                result = searcher.FindAll();
            }

            var userList = (from SearchResult item in result
                            select new AdUser
                            {
                                Name = (item.Properties.Contains("displayname")) ? item.Properties["displayname"][0].ToString() : string.Empty,
                                GlobalId = item.Properties["cn"][0].ToString(),
                                Email = (item.Properties.Contains("mail")) ? item.Properties["mail"][0].ToString() : string.Empty,
                            }).ToList();

            return userList;
        }

        public static List<AdUser> GetAllUsers()
        {
            var entry = new DirectoryEntry("LDAP://global.chiyoda.local");
            SearchResultCollection result;

            using (var searcher = new DirectorySearcher(entry))
            {
                searcher.Asynchronous = true;
                searcher.Filter = "(&(objectClass=person)(!(objectClass=computer)))";
                searcher.PropertiesToLoad.Add("displayname");
                searcher.PropertiesToLoad.Add("cn");
                searcher.PageSize = 1000;
                result = searcher.FindAll();
            }

            var userList = (from SearchResult item in result
                            select new AdUser
                            {
                                Name = (item.Properties.Contains("displayname")) ? item.Properties["displayname"][0].ToString() : string.Empty,
                                GlobalId = item.Properties["cn"][0].ToString()
                            }).ToList();

            return userList;
        }
    }
}
